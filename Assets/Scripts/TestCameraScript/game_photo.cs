﻿//#define USE_LOAD_TEXTURE_THING
using UnityEngine;
using System.Collections;
using System.IO;

public class game_photo : MonoBehaviour
{
    string[] deviceNames;
    int currentCamera = 0;
    Quaternion baseRotation;
    public static WebCamTexture wct = null;
    Renderer photoapply;
    public static Texture2D       texture = null;
    
    GameObject interfacelayer;
	public static Texture		origTexture = null;
    // FB 218572041513575
    WebCamDevice[] devices;

    void Awake()
    {
        baseRotation = transform.rotation;

        interfacelayer = GameObject.Find("interfacelayer");

        photoapply = GameObject.Find("cameraApply").GetComponent<Renderer>();
		origTexture = photoapply.material.mainTexture;

        devices = WebCamTexture.devices;
		deviceNames = new string[devices.Length];
		
		for (int i=0; i< devices.Length; i++)
		{
			deviceNames [i] = devices [i].name;
			if (!devices [i].isFrontFacing)
			{
				currentCamera = i;
			}
		}
		NewWebCamTexture(deviceNames [currentCamera]);
	}

    void Update()
    {

		if (wct.isPlaying)
		{
			var customAngle = wct.videoRotationAngle;
			#if UNITY_ANDROID || UNITY_IOS
			customAngle += 180;
			#endif
			//Debug.Log("video Rotation: " + wct.videoRotationAngle + " customAngle: " + customAngle 
			//          + " videoVerticallyMirrored: " + wct.videoVerticallyMirrored);
			transform.rotation = baseRotation * Quaternion.AngleAxis(customAngle, Vector3.forward);
			
			//if (!Input.GetMouseButton(0))
			{
				var scale = transform.localScale;
				scale.y = devices [currentCamera].isFrontFacing ? -1f : 1f;
				#if UNITY_IOS
				scale.y *= -1;
				#endif
				//scale.y = wct.videoVerticallyMirrored ? -1 : 1;
				transform.localScale = scale;
			}
		}
	}
	
    public bool CanISwap()
    {
        if (deviceNames == null)
            return false;
        return deviceNames.Length > 1;
    }

    public void SwapCamera()
    {
        currentCamera = (currentCamera + 1) % deviceNames.Length;
        NewWebCamTexture(deviceNames [currentCamera]);
    }

    void NewWebCamTexture(string webCamName)
    {
        if (wct)
        {
            wct.Stop();
            Destroy(wct);
            wct = null;
        }
        wct = new WebCamTexture(webCamName, Screen.width, Screen.height, 12);
        wct.Play();
        photoapply.material.mainTexture = wct;
    }

    void OnDestroy()
    {
        if (wct)
        {
            wct.Stop();
        }
    }
}
