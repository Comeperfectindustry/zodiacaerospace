﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TypeGraphe : MonoBehaviour {
	public GameObject Choix1;
	public GameObject Choix2;
	public GameObject Choix3;
	public GameObject Graphe1;
	public GameObject Graphe2;
	public GameObject Graphe3;



	public void OnClick(){
		Graphe1.SetActive (false);
		Graphe2.SetActive (false);
		Graphe3.SetActive (false);
		if (GetComponent<Dropdown> ().value == 0) {
			Choix1.SetActive (true);
			Choix2.SetActive (false);
			Choix3.SetActive (false);
		} else if (GetComponent<Dropdown> ().value == 1) {
			Choix1.SetActive (true);
			Choix2.SetActive (true);
			Choix3.SetActive (false);
		} else if (GetComponent<Dropdown> ().value == 2) {
			Choix1.SetActive (true);
			Choix2.SetActive (true);
			Choix3.SetActive (true);
		} else {
			Choix1.SetActive (false);
			Choix2.SetActive (false);
			Choix3.SetActive (false);
		}
	}
}
