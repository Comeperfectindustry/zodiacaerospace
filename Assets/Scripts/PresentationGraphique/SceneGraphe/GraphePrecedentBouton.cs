﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GraphePrecedentBouton : MonoBehaviour {
	public GameObject Choix1;
	public GameObject Choix2;
	public GameObject Choix3;
	public GameObject Choix4;
	public GameObject MenuDroite;
	public GameObject GrapheAfaireApparaitre;
	public GameObject Graphe;
	public GameObject axe1;
	public GameObject axe2;
	public bool sens;
	// Use this for initialization
	void Awake () {


	}

	public void OnClick(){
		Choix1.GetComponent<Dropdown> ().interactable = sens;
		Choix2.GetComponent<Dropdown> ().interactable = sens;
		Choix3.GetComponent<Dropdown> ().interactable = sens;
		Choix4.GetComponent<Dropdown> ().interactable = sens;
		GrapheAfaireApparaitre.SetActive (sens);
		Graphe.SetActive (!sens);
		MenuDroite.SetActive (!sens);
		axe1.SetActive (sens);
		axe2.SetActive (!sens);
	}
	// Update is called once per frame

}
