﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeValuePC : MonoBehaviour {

	public GameObject PC1;
	public GameObject PC2;
	public GameObject Point;
	public GameObject Point2;
	public GameObject toogle;
	public string st;
	public GameObject bulle;

	public void OnClick(){
		PC1.SetActive (false);
		PC2.SetActive (true);
		Point.GetComponent<Image> ().color = PC2.transform.GetChild (1).GetComponent<Image> ().color;
		Point2.GetComponent<Image> ().color = PC2.transform.GetChild (1).GetComponent<Image> ().color;
		toogle.GetComponent<Toggle> ().isOn = true;
		bulle.GetComponent<Text> ().text = st;

	}

}
