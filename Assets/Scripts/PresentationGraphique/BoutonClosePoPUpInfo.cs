﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoutonClosePoPUpInfo : MonoBehaviour {
	public GameObject Panel;
	public GameObject popinfo;
	public GameObject menu1;
	public GameObject menu2;
	// Use this for initialization
	public void OnClick(){
		menu1.GetComponent<Button> ().interactable = true;
		menu2.GetComponent<Button> ().interactable = true;
		popinfo.SetActive (true);
		Panel.GetComponent<OpenPopUp> ().OnClickClose ();
	}
}
