﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OpenPopUp : MonoBehaviour {
	public GameObject PopUpInfo;
	public GameObject image;
	public GameObject EnsemblePoint;
	public GameObject menu1;
	public GameObject menu2;
	//max dim vaut la hauteur(resp longueur) +2 fois la position local en valeur abs (attention angle pour la suite).
	public Vector2 maxDim;

	void Start(){
		PopUpInfo.SetActive (false);

	}

	public void OnClick(){
		PopUpInfo.SetActive (true);
		image.GetComponent<DeplacementZoomImage> ().ZoomSurZone (-2);
		EnsemblePoint.SetActive (false);
		menu1.GetComponent<Button> ().interactable = false;
		menu2.GetComponent<Button> ().interactable = false;
	}

	public void OnClickClose(){
		PopUpInfo.SetActive (false);
		//EnsemblePoint.SetActive (true);

		image.GetComponent<DeplacementZoomImage> ().ZoomSurZone (-3);

	}
}
