﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Validé : MonoBehaviour {
	public GameObject Encadre1;
	public GameObject Encadre2;
	public GameObject Encadre3;
	public GameObject Rond1;
	public GameObject Rond2;
	public GameObject toggleRed;
	public string st;
	public GameObject bulle;

	public void OnClick(){
		if (GetComponent<Toggle> ().isOn == true) {
			Encadre1.SetActive (false);
			Encadre2.SetActive (true);
			Encadre3.SetActive (false);
			Rond1.GetComponent<Image> ().color = transform.GetChild (0).GetComponent<Image> ().color;
			Rond2.GetComponent<Image> ().color = transform.GetChild (0).GetComponent<Image> ().color;
			toggleRed.GetComponent<Toggle> ().isOn = false;
			bulle.GetComponent<Text> ().text = st;

		}
	}


}
