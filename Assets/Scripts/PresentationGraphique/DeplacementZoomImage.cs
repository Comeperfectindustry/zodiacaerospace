﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeplacementZoomImage : MonoBehaviour {
	public Vector3 diffCentre;
	public bool nouv;
	public bool dedans;
	public float valeurWheel;
	public float currentScale;
	public GameObject listePoint;
	public Vector2 dimInit;
	public Vector3 PosInit;
	public float ratioD;
	public Vector3 distance;
	public bool autozoom;
	public bool positionDepart;
	public bool reinit;
	public Sprite Image1;
	public Sprite Image2;
	public Sprite Image3;
	public Sprite image4;
	public Sprite Image5;
	public float imageFutur;
	public float ImageActuel;
	public bool change;
	public GameObject PointDeControle;
	public GameObject PointDeControle2;
	public GameObject PointDeControle3;
	// Use this for initialization
	void Start () {
		valeurWheel = 0;
		currentScale = 1;
		PosInit = new Vector3 (-393, -131.9f, 0);
		dimInit = new Vector2 (1774, 1336.8f);
		autozoom = false;
		positionDepart = true;
		change = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (dedans && Input.GetMouseButton (0) && Input.touchCount==1) {
			if (nouv) {
				diffCentre = transform.parent.position - Input.mousePosition;
				nouv = false;
			} else {
				transform.parent.position = Input.mousePosition + diffCentre;
				listePoint.transform.position = Input.mousePosition + diffCentre;
			}
		} else if (dedans && valeurWheel != Input.GetAxis ("Mouse ScrollWheel")) {
			valeurWheel = Input.GetAxis ("Mouse ScrollWheel");
			Zoom (Input.GetAxis ("Mouse ScrollWheel"));
		} else  if (Input.touchCount == 2) {
			// Store both touches.
			Touch touchZero = Input.GetTouch (0);
			Touch touchOne = Input.GetTouch (1);
			
			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			Zoom (-deltaMagnitudeDiff*0.015f);
		}
		else{
			nouv=true;
		}

		if (autozoom) {
			StartCoroutine ("zoomProgressif");
			autozoom=false;
		}

	}
	
	public void OnMouseEnter() {
		dedans = true;
	}
	
	public void OnMouseExit() {
		if ( !Input.GetMouseButton(0)) {
			dedans = false;
		}
	}

	void Zoom(float increment) {
		foreach (Transform t in listePoint.transform) {
			t.localPosition=t.localPosition*(1/currentScale);
			t.localScale*=(1/currentScale);

		}
		currentScale += increment;

		if (currentScale >= 5) {
			currentScale = 5;
		} else if (currentScale <= 0.1f) {
			currentScale = 0.1f;
		}
		foreach (Transform t in listePoint.transform) {
			t.localPosition=t.localPosition*currentScale;
			t.localScale*=currentScale;
		}
		transform.parent.localScale = new Vector2(currentScale, currentScale);
	}

	public void ZoomSurZone(float nombre) {
		if (nombre < 0) {
			imageFutur = nombre;

		} else if (nombre == 1) {
			if (ImageActuel < 2) {
				imageFutur = ImageActuel + 1;
			} else {
				return;
			}
		} else if (nombre == 0) {
			if (ImageActuel > 0) {
				imageFutur = ImageActuel-1;
			} else {
				return;
			}
		}
		autozoom = true;

	}

	public void reinitialisation(){
		reinit = true;
		autozoom = true;
	}
	IEnumerator zoomProgressif(){
		if (change) {
			change=false;
			//zoom progressif
			/*float temps = 0.5f;

		if (!positionDepart) {
			Debug.Log ("ici");
			Vector3 distance2=transform.parent.localPosition-PosInit;
			float ratioA=1-transform.parent.localScale.x;
			while (temps>0) {
				
				transform.parent.localPosition-=Time.deltaTime*distance2;
				listePoint.transform.localPosition-=Time.deltaTime*distance2;
				Zoom(Time.deltaTime*ratioA);
				temps-=Time.deltaTime;
				yield return new WaitForSeconds(Time.deltaTime);
			}

		}
		if (!reinit) {
			temps = 1;
			while (temps>0) {

				transform.parent.localPosition -= Time.deltaTime * distance;
				listePoint.transform.localPosition -= Time.deltaTime * distance;
				Zoom (Time.deltaTime * ratioD);
				temps -= Time.deltaTime;
				yield return new WaitForSeconds (Time.deltaTime);
			}
			positionDepart = false;
		} else {
			positionDepart=true;
		}
		yield return null;
	}*/

			//Fade Away
			float temps = 1;
			transform.parent.parent.GetChild (2).GetComponent<Image> ().sprite = transform.parent.GetComponent<Image> ().sprite;
			if (imageFutur == -2) {
				PointDeControle.SetActive(false);
				PointDeControle2.SetActive(false);
				PointDeControle3.SetActive(false);
				ImageActuel = -2;
				transform.parent.GetComponent<Image> ().sprite = image4;
			} else if (imageFutur == -1) {
				PointDeControle.SetActive(false);
				PointDeControle2.SetActive(false);
				PointDeControle3.SetActive(false);
				ImageActuel = -1;
				transform.parent.GetComponent<Image> ().sprite = Image5;
			} else if (imageFutur == 0 || imageFutur == -3) {
				PointDeControle.SetActive(true);
				listePoint.SetActive(true);
				PointDeControle2.SetActive(false);
				PointDeControle3.SetActive(false);
				ImageActuel = 0;
				transform.parent.GetComponent<Image> ().sprite = Image1;
			} else if (imageFutur == 2) {
				listePoint.SetActive(false);
				PointDeControle.SetActive(false);
				PointDeControle2.SetActive(false);
				PointDeControle3.SetActive(true);
				ImageActuel = 2;
				transform.parent.GetComponent<Image> ().sprite = Image3;
			} else {
				listePoint.SetActive(false);
				PointDeControle.SetActive(false);
				PointDeControle2.SetActive(true);
				PointDeControle3.SetActive(false);
				ImageActuel = 1;
				transform.parent.GetComponent<Image> ().sprite = Image2;
			}

			Color c = transform.parent.GetComponent<Image> ().color;
			transform.parent.parent.GetChild (2).localScale = Vector3.one;
			transform.parent.localScale = Vector3.zero;
			c.a = 0;
			transform.parent.GetComponent<Image> ().color = c;
			c = transform.parent.parent.GetChild (2).GetComponent<Image> ().color;
			c.a = 1;
			transform.parent.parent.GetChild (2).GetComponent<Image> ().color = c;
			while (temps>0) {
				Vector3 v = new Vector3 (Time.deltaTime, Time.deltaTime, Time.deltaTime);
				c = transform.parent.parent.GetChild (2).GetComponent<Image> ().color;
				c.a -= Time.deltaTime;
				transform.parent.parent.GetChild (2).GetComponent<Image> ().color = c;
				c = transform.parent.GetComponent<Image> ().color;
				c.a += Time.deltaTime;
				transform.parent.GetComponent<Image> ().color = c;
				transform.parent.parent.GetChild (2).localScale -= v;
				transform.parent.localScale += v;
				temps -= Time.deltaTime;
				yield return new WaitForSeconds (Time.deltaTime);
			}
			change = true;
		}

	}
}
