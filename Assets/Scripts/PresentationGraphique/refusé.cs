﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class refusé : MonoBehaviour {
	public GameObject Encadre1;
	public GameObject Encadre2;
	public GameObject Encadre3;
	public GameObject Rond1;
	public GameObject Rond2;
	public GameObject togglevert;
	public string st;
	public GameObject bulle;
	
	public void OnClick(){
		if (GetComponent<Toggle> ().isOn == true) {
			Encadre1.SetActive (false);
			Encadre2.SetActive (false);
			Encadre3.SetActive (true);
			Rond1.GetComponent<Image> ().color = transform.GetChild (0).GetComponent<Image> ().color;
			Rond2.GetComponent<Image> ().color = transform.GetChild (0).GetComponent<Image> ().color;
			togglevert.GetComponent<Toggle> ().isOn = false;
			bulle.GetComponent<Text> ().text = st;

		}
	}
}
