﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlPopUp1 : MonoBehaviour {
	public GameObject listPoint;
	public GameObject listControl;
	public GameObject pourcentage;
	public GameObject popUplancement;
	public GameObject imageObjet;
	// Use this for initialization
	void Start () {
		listPoint.SetActive (false);
		listControl.SetActive (false);
		pourcentage.GetComponent<Text> ().text = "0%";
		popUplancement.SetActive (true);
		imageObjet.SetActive (false);
	}
	
	// Update is called once per frame
	public void OnclickPopUpClose(){
		listPoint.SetActive (true);
		listControl.SetActive (true);
		pourcentage.GetComponent<Text> ().text = "0%";
		popUplancement.SetActive (false);
		imageObjet.SetActive (true);
	}
}
