﻿using UnityEngine;
using System.Collections;

public class ControleInterface : MonoBehaviour {
	public GameObject interface1;
	public GameObject interface2;
	// Use this for initialization
	void Start () {
		interface1.SetActive (true);
		interface2.SetActive (false);
	}

	public void OnClick(){
		interface1.SetActive (false);
		interface2.SetActive (true);
	}
	// Update is called once per frame

}
