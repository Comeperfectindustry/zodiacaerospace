﻿using UnityEngine;
using System.Collections;

public class DeplacementPopUp : MonoBehaviour {
	public Vector3 diffCentre;
	public bool nouv;
	public bool dedans;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (dedans && Input.GetMouseButton(0)) {
			if(nouv){
				diffCentre=transform.parent.position-Input.mousePosition;
				nouv=false;
			}
			else{
			transform.parent.position=Input.mousePosition+diffCentre;
			}
		}
		else{
			nouv=true;
		}
	}

	public void OnMouseEnter() {
		dedans = true;
	}

	public void OnMouseExit() {
		if ( !Input.GetMouseButton(0)) {
			dedans = false;
		}
	}

}
